package com.angelo.webflux.WebFlux.apiRestController;

import com.angelo.webflux.WebFlux.controllers.OperaControllers;
import com.angelo.webflux.WebFlux.dtos.OpeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(OpeResource.OPERA)
public class OpeResource {
    public static final String OPERA = "/opera";
    public static final String ID = "/{id}";
    private OperaControllers operaControllers;

    @Autowired
    public OpeResource(OperaControllers operaControllers) {
        this.operaControllers = operaControllers;
    }

    @PostMapping(produces = {"application/json"})
    public Mono<ResponseEntity> create(@RequestBody OpeDto opeDto) {
        return this.operaControllers.createOpera(opeDto);
    }

    @GetMapping(value = ID)
    public Mono<OpeDto> readPerson(@PathVariable Long id) {
        return this.operaControllers.findPersonById(id);
    }

    @GetMapping
    public Flux<OpeDto> search() {
        return this.operaControllers.search();
    }

    @PutMapping(value = ID)
    public Mono<ResponseEntity> updatePerson(@PathVariable Long id, @RequestBody OpeDto opeDto) {
        return this.operaControllers.editUser(id, opeDto);
    }
}
