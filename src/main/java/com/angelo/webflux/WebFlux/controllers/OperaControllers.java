package com.angelo.webflux.WebFlux.controllers;
import com.angelo.webflux.WebFlux.documents.Opera;
import com.angelo.webflux.WebFlux.dtos.OpeDto;
import com.angelo.webflux.WebFlux.exceptions.NotFoundException;
import com.angelo.webflux.WebFlux.repositories.OperaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller

public class OperaControllers {
    private OperaRepository operaRepository;
    @Autowired
    private OperaControllers(OperaRepository operaRepository){
        this.operaRepository = operaRepository;
    }
    public Mono<ResponseEntity> createOpera(OpeDto opeDto) {
        Opera opera = new Opera();
        opera.setName(opeDto.getName());
        opera.setType(opeDto.getType());
        opera.setUser(opeDto.getUser());
        opera.setId(opeDto.getId());
        opera.setActive(opeDto.getActive());
        opera.setData(opeDto.getData());
        return this.operaRepository.save(opera).map(callback ->{
            return new ResponseEntity("\"New User\"", HttpStatus.CREATED);
        }).onErrorReturn(new ResponseEntity("\"User do not created\"", HttpStatus.NOT_ACCEPTABLE));
    }
    public Mono<OpeDto> findPersonById(Long id){
        return this.operaRepository.findById(id).switchIfEmpty(Mono.error(new NotFoundException("User" + id)))
                .map(OpeDto::new);
    }
    public Flux<OpeDto> search(){
        return this.operaRepository.findAll().map(OpeDto::new);
    }
    public Mono<ResponseEntity> editUser(Long id, OpeDto opeDto){
        Mono<Opera> opera = this.operaRepository.findById(id)
                .switchIfEmpty(Mono.error(new NotFoundException("User" + id)))
                .map(db -> {
                    db.setName(opeDto.getName());
                    db.setType(opeDto.getType());
                    db.setUser(opeDto.getUser());
                    db.setId(opeDto.getId());
                    db.setActive(opeDto.getActive());
                    db.setData(opeDto.getData());
                    return db;
                });
        return Mono.when(opera).then(this.operaRepository.save(opera.block()).map(callback ->{
            return new ResponseEntity("\"Updated\"",HttpStatus.ACCEPTED);
        }));
    }
}
