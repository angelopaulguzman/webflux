package com.angelo.webflux.WebFlux.dtos;
import com.angelo.webflux.WebFlux.documents.Opera;
public class OpeDto {
    private String name, type, user;
    private Long id;
    private Boolean active;
    private int[] data;


    public OpeDto() {
    }
    public OpeDto(String name, String type, String user,Long id, Boolean active, int[] data) {
        this.name = name;
        this.type = type;
        this.user = user;
        this.id = id;
        this.active = active;
        this.data = data;
    }
    public OpeDto(Opera opera){

        this(
                opera.getName(),
                opera.getType(),
                opera.getUser(),
                opera.getId(),
                opera.getActive(),
                opera.getData()
        );
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public int[] getData() {
        return data;
    }

    public void setData(int[] data) {
        this.data = data;
    }
}