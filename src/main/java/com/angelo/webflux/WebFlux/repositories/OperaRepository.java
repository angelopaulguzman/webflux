package com.angelo.webflux.WebFlux.repositories;

import com.angelo.webflux.WebFlux.documents.Opera;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;

public interface OperaRepository extends ReactiveSortingRepository<Opera,Long> {
}