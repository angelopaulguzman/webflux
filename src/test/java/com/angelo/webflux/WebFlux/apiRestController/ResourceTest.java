package com.angelo.webflux.WebFlux.apiRestController;
import com.angelo.webflux.WebFlux.dtos.OpeDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.reactive.function.BodyInserters;

import static org.junit.jupiter.api.Assertions.*;
@ApiTestConfig
public class ResourceTest {
    @Autowired
    private RestService restService;

    @Test
    void read(){
        OpeDto opeDto = this.restService.restbuildert().get()
                .uri(OpeResource.OPERA+OpeResource.ID, 1)
                .exchange().expectStatus().isOk().expectBody(OpeDto.class)
                .returnResult().getResponseBody();

        assertNotNull(opeDto);
        assertEquals(1, opeDto.getId());
    }

    @Test
    void search(){
        this.restService.restbuildert().get().uri(OpeResource.OPERA).exchange().expectStatus().isOk();
    }

    @Test
    void update(){
        OpeDto opeDto = new OpeDto( );
        String post=
                this.restService.restBuilder()
                        .put().uri(OpeResource.OPERA+OpeResource.ID,1)
                        .body(BodyInserters.fromObject(opeDto))
                        .exchange()
                        .expectStatus().isAccepted()
                        .expectBody(String.class).returnResult().getResponseBody();
        assertNotNull(post);
        assertEquals("\"Updated\"", post);

    }
}
